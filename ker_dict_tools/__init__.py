from .dictDiff import diff as dict_diff
from .dictPaths import get_value as get_value_by_path
from .dictPaths import set_value as set_value_by_path