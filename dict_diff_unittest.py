import unittest
from copy import deepcopy
from dict_tools import (
    dict_diff as diff, 
    set_value_by_path as set_value
)
from testDicts import (
    numLettersDict,
    numLettersList,
    testDiffNLDict,
    testDiffNLList
)


class Test_dictDiff(unittest.TestCase):

    def setUp(self):

        
        self.dct1 = numLettersDict
        self.dct2 = deepcopy(self.dct1)
        self.lst1 = numLettersList
        self.lst2 = deepcopy(self.lst1)
        self.lstTests = testDiffNLList
        self.dctTests = testDiffNLDict

    def tearDown(self):
        self.dct1=None
        self.dct2=None
        self.lst1=None
        self.lst2=None

    def test_diff_raises_TypeError_different_args_type_fail_True(self):
        self.assertRaises(
            TypeError,
            diff,
            dct1=self.dct1,
            dct2=self.lst1,
            fail=True
        )

    def test_diff_returns_False_and_empy_lists_different_args_type_fail_False(self):
        r = diff(self.dct1, self.lst1, fail=False)
        self.assertEqual(r.compared, False)
        self.assertEqual(r.updated, [])
        self.assertEqual(r.removed, [])
        self.assertEqual(r.added, [])

    def test_diff_returns_True_and_empty_lists_with_equal_dicts(self):
        r = diff(self.dct1, self.dct2)
        self.assertEqual(r.compared, True)
        self.assertEqual(r.updated, [])
        self.assertEqual(r.removed, [])
        self.assertEqual(r.added, [])

    def test_diff_returns_True_and_expected_lists_for_different_dicts(self):
        for tst in self.dctTests:
            set_value(self.dct2, tst.path, tst.newVal)
        
        r = diff(self.dct1, self.dct2)
        self.assertEqual(r.compared, True)
        self.assertEqual(len(r.updated), len(self.dctTests))
        self.assertTrue(all(
            [y.newVal in [x.new_value for x in r.updated] for y in self.dctTests]
        ))

    def test_diff_returns_True_and_expected_lists_for_different_lists(self):
        for tst in self.lstTests:
            set_value(self.lst2, tst.path, tst.newVal)
        
        r = diff(self.lst1, self.lst2)
        self.assertEqual(r.compared, True)
        self.assertEqual(len(r.updated), len(self.lstTests))
        self.assertTrue(all(
            [y.newVal in [x.new_value for x in r.updated] for y in self.lstTests]
        ))

if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)
    try: 
        unittest.main(verbosity=2)
    except Exception as e:
        print ("Uncaught exception running unittest.main():", e)
