from collections import namedtuple

nodesStatus = {
    "2": {
        "ui": {
            "k_info": {
                "0" : {"pin_status": 1},
                "1" : {"pin_status": 1},
                "2" : {"pin_status": 1},
                "3" : {"pin_status": 1},
                "4" : {"pin_status": 1},
                "5" : {"pin_status": 1},
                "6" : {"pin_status": 1},
                "7" : {"pin_status": 1},
            },
            "sensors" : {
                "1": {"value": 23},
                "2": {"value": 41}
            }, 
            "motors" : {
                "1": {"value": 2233}
            }

            
        }
    },

    "3": {
        "ui": {
            "sensors" : {
                "1": {"value": 28}
            }, 
            "motors" : {
                "1": {"value": 2233}
            }

            
        }
    }
}

persons={
    "friends":[
        {"name":"luca",
        "interests":[
            {"type":"art",
             "favorites":"Leonardo",
             "practicing":False
             },
            {"type":"informatic",
             "favorites":"Python",
             "practicing":True
             },
            {"type":"matial arts",
             "favorites":"Taekwondo",
             "practicing":False
             },
            ],
        "age":38,
        "job":["bartender","programmer"]
         },
        {"name":"luca",
        "interests":[
            {"type":"art",
             "favorites":"Picasso",
             "practicing":True
             },
            {"type":"informatic",
             "favorites":"Java",
             "practicing":True
             },
            {"type":"matial arts",
             "favorites":"karate",
             "practicing":False
             },
            ],
        "age":42,
        "job":["chef","carpenter"]
         },
        {"name":"Sofia",
        "interests":[
            {"type":"music",
             "favorites":"BonJovi",
             "practicing":True
             },
            {"type":"politics",
             "favorites":"democratic",
             "practicing":True
             },
            {"type":"movies",
             "favorites":"Amelie",
             "practicing":False
             },
            ],
        "age":27,
        "job":["politic"]
         },
        {"name":"marco",
        "interests":[
            {"type":"art",
             "favorites":"Michelangelo",
             "practicing":False
             },
            {"type":"sport",
             "favorites":"swimming",
             "practicing":True
             },
            {"type":"freaks",
             "favorites":"snoat",
             "practicing":False
             },
            ],
        "age":34,
        
        "job":["cop"]
         },
        
    ]
}

cars = [
    {
        "model": "Fiesta",
        "producer": "Ford",
        "owner": ['Luca', 'Luciano'],
        "condition": {
            "cambio": {
                "functioning": True,
                "model": "aaaa",
                "checked": "19-02-2018",
                
            },
            "volante": {
                "functioning": True,
                "model": "bbbb",
                "checked": "19-03-2018",
                
            },
            "freni": {
                "functioning": True,
                "model": "cccc",
                "checked": "19-04-2018",
                
            },
            
        },
        "interni": {
            "sedile": "modA",
            "tappetini": ['antSx', 'antDx', 'posSx', 'posDx'],

        },
        "optionals": [
            'radio', 'abs', 'condizionatore'
        ]

    },

    {
        "model": "Fiat",
        "producer": "Panda",
        "owner": ['Titti'],
        "condition": {
            "cambio": {
                "functioning": False,
                "model": "abab",
                "checked": "15-02-2018",
                
            },
            "volante": {
                "functioning": False,
                "model": "baba",
                "checked": "16-03-2018",
                
            },
            "freni": {
                "functioning": False,
                "model": "cbcb",
                "checked": "17-04-2018",
                
            },
            
        },
        "interni": {
            "sedile": "modB",
            "tappetini": ['antDx', 'posSx', 'posDx'],

        },
        "optionals": [
            
        ]

    },

    {
        "model": "Diablo",
        "producer": "Lamborghini",
        "owner": ['Franco', 'Luciano'],
        "condition": {
            "cambio": {
                "functioning": True,
                "model": "dddd",
                "checked": "19-02-2016",
                
            },
            "volante": {
                "functioning": True,
                "model": "eeee",
                "checked": "19-03-2016",
                
            },
            "freni": {
                "functioning": True,
                "model": "ffff",
                "checked": "19-04-2016",
                
            },
            
        },
        "interni": {
            "sedile": "modA",
            "tappetini": ['antSx', 'antDx'],

        },
        "optionals": [
            'radio', 'abs', 'condizionatore', 'cerchi', 'touchscreen', 'computer'
        ]

    },

    {
        "model": "Corsa",
        "producer": "Opel",
        "owner": ['Massimo'],
        "condition": {
            "cambio": {
                "functioning": True,
                "model": "0000",
                "checked": "14-01-2018",
                
            },
            "volante": {
                "functioning": False,
                "model": "1111",
                "checked": "14-05-2018",
                
            },
            "freni": {
                "functioning": True,
                "model": "2222",
                "checked": "14-09-2018",
                
            },
            
        },
        "interni": {
            "sedile": "modA",
            "tappetini": ['antSx', 'antDx', 'posSx', 'posDx'],

        },
        "optionals": [
            {'type':'radio', "available":True}
        ]

    }


]

numLettersDict = {
    'a':1,
    'b':2,
    'c':3,
    'd': [
        {'A':10},
        {'B':20},
        {'C':30},
    ],
    'e': [
        [
            {
                "AA":'AAAA',
                "BB":'BBBB',
                "CC":'CCCC',
                "DD": ['Z', 'W', 'Y', 'X']
            },
            {
                "AA":'EEEE',
                "BB":'FFFF',
                "CC":'GGGG',
                "DD": ['O', 'P', 'Q', 'R']
            },
            {
                "AA":'EEEE',
                "BB":'BBBB',
                "CC":'GGGG',
                "DD": ['E', 'I', 'O', 'U']
            },
        ]
    ]
}

numLettersList = [
    {
        'a':1,
        'b': {
            'B': {
                'BB':2,
                'CC':3,
                'DD':4,
                'EE':5
            },
            'C': True,
            'F': 0.01,
            'H': 'HI'
        },
        'c': [0,1,2,3,4,5]
    },
    {
        'a':2,
        'b': {
            'B': {
                'BB':20,
                'CC':30,
                'DD':40,
                'EE':50
            },
            'C': True,
            'F': 0.1,
            'H': 'Hi'
        },
        'c': [1,2,3,4,5,0]
    },
    {
        'a':3,
        'b': {
            'B': {
                'BB':200,
                'CC':300,
                'DD':400,
                'EE':500
            },
            'C': False,
            'F': 0.11,
            'H': 'HELLO'
        },
        'c': [1,2,3,4,5,6]
    },
]

testTuple = namedtuple('test_tuple', [
    'path',
    'oldVal',
    'newVal'
])

testDiffNLDict = [
    testTuple(
        path=['d', 0, 'A'],
        oldVal=10,
        newVal=20
        ),
    testTuple(
        path=['e', 0, 1, 'BB'],
        oldVal='FFFF',
        newVal='FUSE'
        ),
    testTuple(
        path=['e', 0, 2, 'DD', 1],
        oldVal='I',
        newVal='III'
        ),
    testTuple(
        path=['e', 0, 2, 'CC'],
        oldVal='GGGG',
        newVal='WAS A LIST'
        )
    
]

testDiffNLList = [
    testTuple(
        path=[0, 'a'],
        oldVal=1,
        newVal=['1']
        ),
    
    testTuple(
        path=[1, 'b', 'B', 'CC'],
        oldVal=30,
        newVal=31
        ),
    
    testTuple(
        path=[2, 'b', 'C'],
        oldVal=False,
        newVal=True
        ),
    
    
]