from testDicts import (
    nodesStatus,
    persons,
    cars
)
import logging
logging.basicConfig(level=logging.DEBUG)

from copy import deepcopy
from pprint import pprint as pp
from ker_dict_tools import (
    get_value_by_path as get_value,
    set_value_by_path as set_value,
    dict_diff as diff
    )
marco_practicing_michelangelo = [
    'friends',
    {"name":"marco"},
    "interests",
    [{"type":"art"},{"favorites":"Michelangelo"}],
    "practicing"
    ]
luca_job = [
    'friends',
    {"name":"luca"},
    "job"
    ]
luca_interests_0_fav = [
    'friends', 
    {"name":"luca"}, 
    "interests", 
    0, 
    'favorites']
all_practicing_informatic = [
    'friends',
    '*',
    'interests',
    [{"type":'informatic'},{"practicing":True}]
    ]
    

cars_radio_available_1 = ['*', 'optionals', 'radio'] 

cars_spt_available_1 = ['*', 'optionals', 'spt'] 

prs1 = deepcopy(persons)
found, res = get_value(persons, [
    'friends',
    {"name":"marco"},
    "interests",
    [{"type":"art"},{"favorites":"Michelangelo"}],
    "practicing"
    ])
print (set_value(prs1, res[0].path, True))
print (set_value(prs1, res[0].path[:-1], None))
pp( diff(persons, prs1))
